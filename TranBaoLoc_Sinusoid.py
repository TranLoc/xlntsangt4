#x ( t ) = A c o s ( 2 πft + f )
#code 1
import numpy as np
import matplotlib.pyplot as plt
#add module numpy và matplotlib vào
A = .8
#A = 0.8
f = 5
#f = 5
t = np.arange(0,1,.01)
#tạo ra 1 mảng từ 0 đến 0.99 với giá trị các phần tử trong mảng cách nhau 0.01 đơn vị
phi = np.pi/4
#khởi tạo biến phi có giá trị bằng pi chia 4
x = A*np.cos(2*np.pi*f*t + phi)
#khởi tạo biến x với giá trị bằng A*cos(omega*t +phi) với omega = 2*pi*f
plt.plot(t,x)
#phác họa đồ thị hình sin theo công thức x, với giá trị nằm trong mảng t
plt.axis([0,1,-1,1])
#Lệnh axis() nhận giá trị trên theo danh sách [xmin, xmax, ymin, ymax] và biểu thị nó trên các trục số
plt.xlabel('time in seconds')
#thêm ghi chú cho trục x
plt.ylabel('amplitude')
#thêm ghi chú cho trục y
plt.show()
#hiển thị dạng đồ thị


#code 2
A = .65
#khởi tạo biến A = 0.65
fs = 100
#khởi tạo biến fs=100
samples = 100
#khởi tạo biến samples = 100
f = 5
#khởi tạo biến f=5
phi = 0
#khởi tạo biến phi = 0
n = np.arange(samples)
#gán n bằng 1 mảng 100 giá trị từ 0  đến 99
T = 1.0/fs
# Remember to use 1.0 and not 1!
#gán giá trị T bằng 1.0 chia fs
y = A*np.cos(2*np.pi*f*n*T + phi)
#gán công thức hàm cos cho y
plt.plot(y)
#phác họa đồ thị y
plt.axis([0,100,-1,1])
#trục x có giá trị từ 0 đến 100, và tryc5 y có giá trị từ -1 đến 1
plt.xlabel('sample index')
#thêm ghi chú cho trục x
plt.ylabel('amplitude')
#thêm ghi chú cho trục y
plt.show()
#hiển thị

#code 3
f = 3
#khởi tạo biến f =3
t = np.arange(0,1,.01)
#tạo 1 mảng t có gái trị từ 0 đến 0.99 với các giá trị cách nhau 0.01
phi = 0
#gán phi = 0
x = np.exp(1j*(2*np.pi*f*t + phi))
#gán công thức cho x = ej(2πft+ϕ)
xim = np.imag(x)
#gán phần ảo của x vào biến xim
plt.figure(1)
#khởi tạo 1 figure
plt.plot(t,np.real(x))
#vẽ phần thực
plt.plot(t,xim)
#vẽ phần ảo
plt.axis([0,1,-1.1,1.1])
#xác định giá trị trục x từ 0 đến 1 và y từ -1.1 đến 1.1
plt.xlabel('time in seconds')
#thêm ghi chú cho trục x
plt.ylabel('amplitude')
#thêm ghi chú cho trục y
plt.show()
#hiển thị dạng đồ thị sau khi phác họa



#code 4
f = 3
#khởi tạo biến f=3
N = 64
#khởi tạo biến N=64
n = np.arange(64)
#tạo 1 mảng t có gái trị từ 0 đến 64
phi = 0
#khởi tạo biến phi=0
x = np.exp(1j*(2*np.pi*f*n/N + phi))
#gán công thức cho x=ej(2πfn/N+ϕ).
xim = np.imag(x)
#xác định phần ảo của x và gán vào biến xim
plt.figure(1)
#khởi tạo 1 figure
plt.plot(n,np.real(x))
#vẽ phần thực của x với giá trị nằm trong vùng n
plt.plot(n,xim)
#vẽ phần ảo của x với giá trị nằm trong vùng n
plt.axis([0,samples,-1.1,1.1])
#xác định giá trị trục x từ 0 đến 60 và y từ -1.1 đến 1.1
plt.xlabel('sample index')
#thêm ghi chú cho trục x
plt.ylabel('amplitude')
#thêm ghi chú cho trục y
plt.show()
#hiển thị dạng đồ thị sau khi phác họa


#code 5
N = 44100
#khởi tạo biến N=44100
f = 440
#khởi tạo biến f=440
fs = 44100
#khởi tạo biến fs=44100
phi = 0
#khởi tạo biến phi
n = np.arange(N)
#khởi tạo arr
x = A*np.cos(2*np.pi*f*n/N + phi)
#scipy.io.wavfile.write(filename, rate, data)[source]
from scipy.io.wavfile import write
#Write a numpy array as a WAV file.
#filename:Output wav file.
#rate:The sample rate (in samples/sec).
#data:A 1-D or 2-D numpy array of either integer or float data-type.
write('sine440_1sec.wav', 44100, x)