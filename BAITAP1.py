print('BT1')
# câu a
s = "Hi John, welcome to python programming for beginner!"
b = 'python' in s
print(b)
# câu b
s1 = s.find('John')
print(s1)
# câu c
c = s.count('o')
print(c)
# câu d
d = len(s)
print(d)

print('BT2')
# cau a
l = [23, 4.3, 4.2, 31, 'python', 1, 5.3, 9, 1.7]
l.remove('python')
print(l)
# câu b
l.sort(reverse=True)
print(l)
l.sort(reverse=False)
print(l)
# câu c
B = 4.2 in l
print(B)

print('BT3')
A = {1, 2, 3, 4, 5, 7}
B = {2, 4, 5, 9, 12, 24}
C = {2, 4, 8}
# câu 1 + 2
A.update([2, 4, 8])
B.update([2, 4, 8])  # update thêm giá trị
print(A)
print(B)
# câu 3: toán tử giao (intersection)
print(A & B)
# câu 4: toán tử tập hợp (union)
print(A | B)
# câu 5: toán tử diff
print(A - B)

L1 = len(A)
L2 = len(B)
print(L1, L2)

L3 = max(A)
L4 = min(A)
print(L3, L4)
L5 = max(B)
L6 = min(B)
print(L5, L6)

print('BT4')
t = (1, 'python', [2, 3], (4, 5))
print(type(t))
print(tuple(t))  # khởi tạo 1 tuple
print(t[-1])
t1 = ([2, 3],)  # add list[2,3] vào tuple
t += t1
print(t)
t2 = list(t)  # chuyển đổi tuple thành list
print(t2)
print(type(t2))
# xóa list khỏi tuple CHƯA BIẾT

print('BT5')
dic1 = [(1, 10), (2, 20)]
dic2 = [(3, 30), (4, 40)]
dic3 = [(5, 50), (6, 60)]
print(dict(dic1))
print(dict(dic2))
print(dict(dic3))
dictotal = dict(dic1 + dic2 + dic3)
print(dictotal)
print(type(dictotal))

i = 1
while i <= 15:
    print([(i, i ** i)])
    i += 1

Q = {'a': 1, 'b': 2, 'c': 3, 'd': 4}
# hàm sorted dùng sắp xếp key trong dict
print(sorted(Q))

string = 'tran bao loc'
for i in string:
    if string.count(i) == 1:
        print([i], ':', string.count(i))
# thiếu điều kiện để tránh lặp chữ cái


# BÀI 6


print('BT7')
nl = ['Python', 'Bill', 'Trump', 'Java', 'Pascal']
para = '''speaking at the white house, trump said the nation is overcome with shock, horror and sorrow. 
according to bill gate who said that python nowadays is great programming language for data scientist 
while java still be the first for mobile apps. pascal was dead. other news, please refer here...'''
m = para.title()  # in hoa
print(m)
# đếm

print('BT8')
import random

# tạo ngẫu nhiên 1 list
j = 10
i = 1
while (i <= j):
    gh = random.uniform(1, 50)
    print(gh)
    i += 1

# BÀI 9
print('BT10')
A = [[1, 2, 3], [4, 5, 6]]
print('\r')
print(A[0])
print(A[1])
B = [[2, 3], [4, 5], [6, 7]]
print(B[0])
print(B[1])
print(B[2])
C = [[1 * 2 + 2 * 4 + 3 * 6, 1 * 3 + 2 * 5 + 3 * 7], [4 * 2 + 5 * 4 + 6 * 6, 4 * 3 + 5 * 5 + 6 * 7]]
print(C[0])
print(C[1])

print('BT11')
# function map
# cấu trúc: map(x,y....)
# với:
# x là hàm xử lý logic
# y có thể là list, dic cần lặp xử lý
L = [(1, 2, 3), (2, 5, 3), (2, 4, 8), (3, 6, 4)]


# khai báo funtion logic
def divine(x):
    return x / 10


result0 = map(divine, L[0])
result1 = map(divine, L[1])
result2 = map(divine, L[2])
print(list(result0))
print(list(result1))
print(list(result2))

print('BT12')
menu = {'item1': 25, 'item2': 50, 'item3': 75}
print('BẢNG GIÁ ĐƠN HÀNG')
name = menu['item1']
name1 = menu['item2']
name2 = menu['item3']
print(name, name1, name2)
